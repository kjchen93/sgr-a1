// Controlling multiple servo motors,
// by updating them every 10ms or 20ms.
//
// Version 1, July 25, 2021 by Koepel

#include <Servo.h>
#include <LiquidCrystal.h>

LiquidCrystal lcd(12, 11, 10, 9, 8, 7);

int x = 90;
int y = 90;

unsigned long previousMillis;
const unsigned long interval = 10;   // 10 or 20 milliseconds are common values

Servo servo3;
Servo servo4;

int count700ms = 0;    // count to 700ms for first servo motor

#define VERT_PIN A0
#define HORZ_PIN A1

void setup() 
{
  servo3.attach(6);
  servo4.attach(5);
  pinMode(VERT_PIN, INPUT);
  pinMode(HORZ_PIN, INPUT);
  pinMode(4, INPUT_PULLUP);
  pinMode(13, OUTPUT);
  lcd.begin(16, 2);
}

void loop() 
{
  unsigned long currentMillis = millis();

  if( currentMillis - previousMillis >= interval)
  {
    previousMillis = currentMillis;

    // ------------------------------------------
    // Joystick
    // ------------------------------------------

    int horz = analogRead(HORZ_PIN);
    int vert = analogRead(VERT_PIN);
    if (vert < 700) {
       y = min(y + 1, 180);
    }
    if (vert > 180) {
       y = max(y - 1, 0);
    }
    if (horz > 700) {
       x = min(x + 1, 180);
    }
      if (horz < 180) {
       x = max(x - 1, 0);
    }
    //int angle3 = map( horz, 0, 1023, 0, 180);
    servo3.write(x);
    servo4.write(y);

    // ------------------------------------------
    // Buttons
    // ------------------------------------------
    bool up = false;
    if( digitalRead( 3) == LOW){
      y += 1;
      servo4.write(y);
    }
    if( digitalRead( 2) == LOW){
      y -= 1;
      servo4.write(y);
    }
    if( digitalRead( 1) == LOW){
      x += 1;
      servo3.write(x);
    }
    if( digitalRead( 0) == LOW){
      x -= 1;
      servo3.write(x);
    }
    lcd.setCursor(0,0);
    lcd.print("x="); // Print phrase "BPM: "
    lcd.print(x);
    lcd.print(" ");
    lcd.print("y=");
    lcd.print(y);
    lcd.print(" ");
    delay(10);

    // ------------------------------------------
    // Fire Sound
    // ------------------------------------------
    
    if (digitalRead(4) == LOW){
      lcd.setCursor(0,1);
      tone(13, 132);
      lcd.print("Bam!");
      delay(40);
      noTone(13);
      lcd.setCursor(0,1);
      lcd.print("    ");
    }
  }
}
