# SGR-A1

A design of software and hardware for a SGR-A1 turrent system developed in wokwi. The turrent consists of two servo engines
that controlles the yaw (x) and pitch (y) angle of the turrent.  The angles can be controlled with either push buttons or
by joystick. The red button activates the firearm of the turrent and it's response is shown in the LCD display indicated by
the word *Bam!* and a sound is genereted through the buzzer. The angles are also displayed respectively on the LCD display.

![](./imgs/turrent.jpg)

## Author

[Kai Chen](gitlab.com/kjchen93)